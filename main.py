from flask import render_template

from app.main import createApp
from config.settings import Config
from app.routers.example import example


app = createApp(config=Config)

app.register_blueprint(example)

@app.errorhandler(404)
def error404(e):
    return render_template('layout/404.html', e=e)

if __name__ == '__main__':
    app.run(port=6543)