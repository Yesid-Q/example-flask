from flask import Flask

def createApp(config):
    app: Flask = Flask(__name__)

    app.config.from_object(config())

    app.config['UPLOAD_FOLDER'] = 'app/static/uploads/'

    return app  