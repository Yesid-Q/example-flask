from functools import wraps
from flask import  flash, redirect, request, url_for

def example_decorator(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if request.method == 'GET':
            flash('no se puede ingresar a esta pagina')
            return redirect(url_for('example.index'))
        
        nombre = request.form.get('nombre') or None
        apellido = request.form.get('apellido') or None

        if nombre is None or apellido is None:
            return redirect(url_for('example.index'))
        return f(*args, **kwargs)
    return wrapper