from flask import Blueprint, render_template, session, request

from app.requests.example import example_decorator

example = Blueprint('example', __name__ , url_prefix='/example')


@example.route('/', methods=['GET'])
@example_decorator
def index():
    return render_template('example/index.html')